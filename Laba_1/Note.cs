﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laba_1
{
    class Note
    {
        public string lastName { get; set; }
        public string name { get; set; }
        public string serName { get; set; }
        public string numberPhone { get; set; }
        public string country { get; set; }
        public string birthday { get; set; }
        public string organisation { get; set; }
        public string position { get; set; }
        public string otherNotes { get; set; }

        public Note(string lastName, string name, string serName, string numberPhone, string country, string birthday, string organisation, string position, string otherNotes)
        {
            this.lastName = lastName;
            this.name = name;
            this.serName = serName;
            this.numberPhone = numberPhone;
            this.country = country;
            this.birthday = birthday;
            this.organisation = organisation;
            this.position = position;
            this.otherNotes = otherNotes;
        }
      

        public override string ToString()
        {
            return $"Фамилия:  {this.lastName}, Имя:  {this.name}, Отчество:  {this.serName}, Страна:  {this.country}, " +
                $"Телефон:  {this.numberPhone}, День рождения:  {this.birthday}, Имя огранизации:  {this.organisation}," +
                $"Должность  {this.position}, Прочие заметки  {this.otherNotes}";
        }

    }
}
