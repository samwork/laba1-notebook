﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Laba_1
{
    class Notebook
    {
        static List<Note> noteBook = new List<Note>();
        static object locker = new object();
        static int input = 0;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("\nДобро пожаловать в справочник\n");

                Console.WriteLine("Чтобы создать новую запись в реестре, нажмите '1'\n" +
                                  "Чтобы редактировать уже созданную запись, нажмите '2'\n" +
                                  "Чтобы удалить созданную запись, нажмите '3'\n" +
                                  "Чтобы просмотреть созданные учетные записи, нажмите '4'\n" +
                                  "Чтобы просмотреть все созданные учетные записи, с краткой информацией (Фамилия, Имя, Номер телефона), нажмите '5'");

                try
                {
                    input = Int32.Parse(Console.ReadLine());
                    while (input > 5 || input < 1)
                    {
                        Console.WriteLine("Ошибка ввода данных\nПожалуйста, введите значение от 1 до 5");
                        input = Int32.Parse(Console.ReadLine());
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка ввода данных\nПожалуйста, введите значение от 1 до 5");
                    Console.ReadKey();
                    Console.Clear();
                }

                switch (input)
                {
                    case 1:
                        Console.Write("Фамилия: ");
                        string lastName = Console.ReadLine();

                        Console.Write("Имя: ");
                        string name = Console.ReadLine();

                        Console.Write("Отчество: (поле не является обязательным): ");
                        string serName = Console.ReadLine();

                        Console.Write("Страна: ");
                        string country = Console.ReadLine();

                        Console.Write("Номер телефона: ");
                        string numberPhone = Console.ReadLine();

                        Console.Write("День рождения: (поле не является обязательным): ");
                        string birthday = Console.ReadLine();

                        Console.Write("Имя организации: (поле не является обязательным): ");
                        string organisation = Console.ReadLine();

                        Console.Write("Должность: (поле не является обязательным): ");
                        string position = Console.ReadLine();

                        Console.Write("Прочие заметки: (поле не является обязательным): ");
                        string otherNotes = Console.ReadLine();

                        lock (locker)
                        {
                            noteBook.Add(new Note(lastName, name, serName, country, numberPhone, birthday, organisation, position, otherNotes));
                        }
                        Console.WriteLine("Запись была успешно создана");
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 2:

                        if (noteBook.Count == 0)
                        {
                            Console.WriteLine("В справочнике нет созданных записей");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else
                        {
                            Console.WriteLine("Выберете запись для редактирования\n");
                            try
                            {
                                input = Int32.Parse(Console.ReadLine());
                                while (input > noteBook.Count || input < 0)
                                {
                                    Console.WriteLine("Ошибка ввода данных ");
                                    input = Int32.Parse(Console.ReadLine());
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка ввода данных");
                                Thread.Sleep(630);
                                Console.Clear();
                                break;
                            }

                            var selectNote = noteBook.ElementAt(input - 1);

                            Console.WriteLine(selectNote);

                            Console.WriteLine();
                            Console.WriteLine("Что меняем: 1. Фамилия 2. Имя 3. Отчество 4. Страна 5. Телефон " +
                                                          "6. День рождения 7. Имя организации 8. Должность 9. Прочие заметки");

                            try
                            {
                                input = Int32.Parse(Console.ReadLine());
                                while (input > 9 || input < 0)
                                {
                                    Console.WriteLine("Ошибка ввода данных ");
                                    input = Int32.Parse(Console.ReadLine());
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Ошибка ввода данных");
                                Thread.Sleep(630);
                                Console.Clear();
                                break;
                            }

                            switch (input)
                            {
                                case 1:
                                    Console.Write("Новая фамилия: ");
                                    string lastNameEdit = Console.ReadLine();
                                    selectNote.lastName = lastNameEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 2:
                                    Console.Write("Новое имя: ");
                                    string nameEdit = Console.ReadLine();
                                    selectNote.name = nameEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 3:
                                    Console.Write("Новое отчество: ");
                                    string serNameEdit = Console.ReadLine();
                                    selectNote.serName = serNameEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 4:
                                    Console.Write("Новая страна: ");
                                    string countryEdit = Console.ReadLine();
                                    selectNote.country = countryEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 5:
                                    Console.Write("Новый телефон: ");
                                    string phoneNumberEdit = Console.ReadLine();
                                    selectNote.numberPhone = phoneNumberEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 6:
                                    Console.Write("Новая дата рождения: ");
                                    string birthdayEdit = Console.ReadLine();
                                    selectNote.birthday = birthdayEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 7:
                                    Console.Write("Новое имя организации: ");
                                    string organisationEdit = Console.ReadLine();
                                    selectNote.organisation = organisationEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 8:
                                    Console.Write("Новая должность: ");
                                    string positionEdit = Console.ReadLine();
                                    selectNote.position = positionEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                case 9:
                                    Console.Write("Новые прочие заметки: ");
                                    string otherNotesEdit = Console.ReadLine();
                                    selectNote.otherNotes = otherNotesEdit;
                                    Console.WriteLine("Запись была успешно изменена\n");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                            }
                        }
                        break;

                    case 3:

                        Console.WriteLine("Какую учетную запись удаляем?");
                        try
                        {
                            input = Int32.Parse(Console.ReadLine());
                            while (input > noteBook.Count || input < 0)
                            {
                                Console.WriteLine("Ошибка ввода данных ");
                                input = Int32.Parse(Console.ReadLine());
                            }
                            noteBook.RemoveAt(input - 1);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Ошибка ввода данных");
                            Thread.Sleep(630);
                            Console.Clear();
                            break;
                        }

                        Console.WriteLine("Запись успешно удалена");
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 4:
                        if (noteBook.Count == 0)
                            Console.WriteLine("В справочнике нет созданных записей");
                        foreach (var note in noteBook)
                        {
                            if (note == null)
                                Console.WriteLine("В справочнике нет созданных записей");
                            Console.WriteLine(note);
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 5:
                        if (noteBook.Count == 0)
                            Console.WriteLine("В справочнике нет созданных записей");
                        foreach (var note in noteBook)
                        {
                            Console.WriteLine($"Фамилия:  {note.lastName}, Имя:  {note.name}, Телефон:  {note.numberPhone}");
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }
    }
}
